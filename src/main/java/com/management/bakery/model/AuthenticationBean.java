/**
 *
 */
package com.management.bakery.model;

import lombok.Data;

/**
 * @author jessy.sitcharn
 *
 */
@Data
public class AuthenticationBean {

	private String message;

	public AuthenticationBean(String message) {
		this.message = message;
	}

}
