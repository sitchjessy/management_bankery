/**
 *
 */
package com.management.bakery.model.dto;

import lombok.Data;
import lombok.With;

/**
 * @author jessy.sitcharn
 *
 */
@Data
@With
public class UserDto {

	/* FirstName. */
	private String firstName;
	/* LastName. */
	private String lastName;
	/* Email. */
	private String email;
	/* Address. */
	private String address;
	/* Phone number. */
	private String phoneNumber;
	/* Date of employment. */
	private java.util.Date startDate;
	/* Password. */
	private String password;

	/**
	 * Default constructor.
	 */
	public UserDto() {
	}

	/**
	 * Constructor using fields.
	 *
	 * @param firstName   the firstName.
	 * @param lastName    the lastName.
	 * @param email       the email.
	 * @param address     the address.
	 * @param phoneNumber the phone number.
	 * @param startDate   the date of employment.
	 * @param password    the password.
	 */
	public UserDto(String firstName, String lastName, String email, String address, String phoneNumber,
			java.util.Date startDate, String password) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.address = address;
		this.phoneNumber = phoneNumber;
		this.startDate = startDate;
		this.password = password;
	}

}
