/**
 *
 */
package com.management.bakery.model.entities;

import lombok.Getter;

/**
 * @author jessy.sitcharn
 *
 */
//@Entity
@Getter
public enum Role {
//	GERANT(1, "GERANT"), EMPLOYE(2, "EMPLOYE"), BOULANGER(3, "BOULANGER");
	GERANT("GERANT"), EMPLOYE("EMPLOYE"), BOULANGER("BOULANGER");

////	@Id
//	@GeneratedValue
//	/* TODO */
//	private final int id;

	/* TODO */
	private final String role;

	/**
	 * TODO
	 *
	 * @param id   TODO
	 * @param role TODO
	 */
//	private Role(int id, String role) {
//		this.id = id;
//		this.role = role;
//	}
	private Role(String role) {
		this.role = role;
	}

}
