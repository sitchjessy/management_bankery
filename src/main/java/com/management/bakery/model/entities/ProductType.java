package com.management.bakery.model.entities;

import lombok.Getter;

/**
 * Enum for Type of Product. Four type of types :
 * BOULANGERIE,CONFISERIE,BOISSON,VIENOISERIE.
 *
 * @author jessy.sitcharn
 */
@Getter
public enum ProductType {
	BOULANGERIE("BOULANGERIE"), CONFISERIE("CONFISERIE"), BOISSON("BOISSON"), VIENOISERIE("VIENOISERIE");

	/** TODO */
	private String productType;

	/**
	 * Constructor using fields.
	 *
	 * @param productType the type of product.
	 */
	private ProductType(String productType) {
		this.productType = productType;
	}

}
