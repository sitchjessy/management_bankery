package com.management.bakery.model.entities;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.Data;

/**
 * Class representing the table USER.
 *
 * @author jessy.sitcharn
 */
@Entity(name = "Users")
@Data
public class UserEntity {

	@Id
	@GeneratedValue
	/* Identifier. */
	private int id;
	/* FirstName. */
	private String firstName;
	/* LastName. */
	private String lastName;
	/* Email. */
	private String email;
	/* Address. */
	private String address;
	/* Phone number. */
	private String phoneNumber;
	/* Date of employment. */
	private java.util.Date startDate;
	/* Password. */
	private String password;
	/* Role. */
	@Enumerated(EnumType.STRING)
	private Role role;

	/**
	 * Default constructor.
	 */
	public UserEntity() {
	}

	/**
	 * Constructor using fields.
	 *
	 * @param id          Identifier.
	 * @param firstName   FirstName.
	 * @param lastName    LastName.
	 * @param email       Email.
	 * @param address     Address.
	 * @param phoneNumber Phone Number.
	 * @param date        Date of employment.
	 * @param password    Password.
	 * @param role        Role.
	 */
	public UserEntity(int id, String firstName, String lastName, String email, String address, String phoneNumber,
			java.util.Date date, String password, Role role) {
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.address = address;
		this.phoneNumber = phoneNumber;
		this.startDate = date;
		this.password = password;
		this.role = role;
	}

}
