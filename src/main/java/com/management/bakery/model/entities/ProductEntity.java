package com.management.bakery.model.entities;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.Data;

/**
 * Class representing the table PRODUCT.
 *
 * @author jessy.sitcharns
 */
@Data
@Entity(name = "Product")
public class ProductEntity {

	/** Identifier. */
	@Id
	@GeneratedValue
	private int id;

	/** Name of the product. */
	private String name;

	/** Price of the product. */
	private BigDecimal price;

	/** Type of product. */
	@Enumerated(EnumType.STRING)
	private ProductType productType;

	/**
	 * Default constructor.
	 */
	public ProductEntity() {
	}

	/**
	 * Constructor using fields.
	 *
	 * @param id          the identifier.
	 * @param name        the name of the product.
	 * @param price       the price of the product.
	 * @param productType the type of product {@link ProductType}.
	 */
	public ProductEntity(int id, String name, BigDecimal price, ProductType productType) {
		this.id = id;
		this.name = name;
		this.price = price;
		this.productType = productType;
	}

}
