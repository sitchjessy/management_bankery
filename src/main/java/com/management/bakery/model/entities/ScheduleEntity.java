package com.management.bakery.model.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;

/**
 * Class representing the table SCHEDULE.
 *
 * @author jessy.sitcharn
 */
@Data
@Entity(name = "Schedule")
public class ScheduleEntity {

	/** The identifier. */
	@Id
	@GeneratedValue
	private int id;

	/** Day of work */
	@Temporal(TemporalType.DATE)
	private java.util.Date date;

	/** User who worked. */
	@ManyToOne
	private UserEntity userEntity;

	/**
	 * Default constructor.
	 */
	public ScheduleEntity() {
	}

	/**
	 * Constructor using fields.
	 *
	 * @param id         the identifier.
	 * @param date       Day of work.
	 * @param userEntity User who worked.
	 */
	public ScheduleEntity(int id, java.util.Date date, UserEntity userEntity) {
		this.id = id;
		this.date = date;
		this.userEntity = userEntity;
	}

}
