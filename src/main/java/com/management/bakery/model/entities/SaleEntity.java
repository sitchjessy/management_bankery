package com.management.bakery.model.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;

/**
 * Class representing the table SALE.
 *
 * @author jessy.sitcharn
 */
@Data
@Entity(name = "Sale")
public class SaleEntity {

	/** Identifier. */
	@Id
	@GeneratedValue
	private int id;

	/** Date of the sale. */
	@Temporal(TemporalType.DATE)
	private java.util.Date date;

	/** User who make the sale. */
	@OneToOne
	private UserEntity userEntity;

	/** The product sold. */
	@OneToOne
	private ProductEntity productEntity;

	/**
	 * Default constructor.
	 */
	public SaleEntity() {
	}

	/**
	 *
	 * Constructor using fields.
	 *
	 * @param id            the identifier.
	 * @param date          date of the sale.
	 * @param userEntity    user who make the sale.
	 * @param productEntity the product sold.
	 */
	public SaleEntity(int id, java.util.Date date, UserEntity userEntity, ProductEntity productEntity) {
		this.id = id;
		this.date = date;
		this.userEntity = userEntity;
		this.productEntity = productEntity;
	}

}
