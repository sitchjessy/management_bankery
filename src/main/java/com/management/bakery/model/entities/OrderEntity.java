/**
 *
 */
package com.management.bakery.model.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;

/**
 * Class representing the table ORDER.
 *
 * @author jessy.sitcharn
 *
 */
@Entity(name = "Order_Table")
@Data
public class OrderEntity {

	/** Identifier. */
	@Id
	@GeneratedValue
	private int id;

	/** Date of the order. */
	@Temporal(TemporalType.DATE)
	private java.util.Date date;

	/** Number of products ordered. */
	private int ordered;

	/** Product ordered. */
	@OneToOne
	private ProductEntity product;

	/**
	 * Default constructor.
	 */
	public OrderEntity() {
	}

	/**
	 * Constructor using fields.
	 *
	 * @param id      the identifier.
	 * @param date    date of the order.
	 * @param ordered number of products ordered.
	 * @param product {@code ProductEntity} ordered.
	 */
	public OrderEntity(int id, java.util.Date date, int ordered, ProductEntity product) {
		this.id = id;
		this.date = date;
		this.ordered = ordered;
		this.product = product;
	}

}
