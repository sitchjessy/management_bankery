/**
 *
 */
package com.management.bakery.model;

import java.io.Serializable;

import lombok.Getter;

/**
 * @author jessy.sitcharn
 *
 */
@Getter
public class JwtResponse implements Serializable {

	/** TODO */
	private static final long serialVersionUID = -7734076483266622848L;
	/** jwtToken */
	private final String jwtToken;

	/**
	 * Constructor using fields.
	 *
	 * @param jwtToken the jwtTokn.
	 */
	public JwtResponse(String jwtToken) {
		this.jwtToken = jwtToken;
	}

}
