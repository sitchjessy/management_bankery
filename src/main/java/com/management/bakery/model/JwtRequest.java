/**
 *
 */
package com.management.bakery.model;

import java.io.Serializable;

import lombok.Data;

/**
 * @author jessy.sitcharn
 *
 */
@Data
public class JwtRequest implements Serializable {

	/** TODO */
	private static final long serialVersionUID = -3999657502364891991L;

	/** Username */
	private String username;

	/** Password */
	private String password;

	/**
	 * Constructor using fields.
	 *
	 * @param username the username.
	 * @param password the password.
	 */
	public JwtRequest(String username, String password) {
		this.username = username;
		this.password = password;
	}

}
