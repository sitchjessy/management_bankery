package com.management.bakery.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.management.bakery.model.entities.OrderEntity;

/**
 * DAO for {@code OrderEntity}.
 *
 * @author jessy.sitcharn
 *
 */
public interface OrderRepository extends JpaRepository<OrderEntity, Integer> {

	/**
	 * Get the list of {@code OrderEntity} for a date;
	 *
	 * @param date the date.
	 *
	 * @return a {@code List}<{@link OrderEntity}>.
	 */
	List<OrderEntity> findByDate(java.util.Date date);

	/**
	 * TODO
	 *
	 * @param startDate Start date for the period.
	 * @param endDate   End Date for the period.
	 * @return a {@code List}<{@link OrderEntity}>.
	 */
	List<OrderEntity> findAllByDateBetween(java.util.Date startDate, java.util.Date endDate);

	/**
	 * TODO
	 *
	 * @param productEntity
	 * @return
	 */
//	@Query(value = "SELECT * FROM ORDER " + "WHERE PRODUCT_ID = :productId", nativeQuery = false)
//	List<OrderEntity> findByProductId(@Param("productId") Integer productId);

}
