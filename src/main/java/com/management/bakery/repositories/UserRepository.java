/**
 *
 */
package com.management.bakery.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.management.bakery.model.entities.Role;
import com.management.bakery.model.entities.UserEntity;

/**
 * DAO for {@code UserEntity}.
 *
 * @author jessy.sitcharn
 *
 */
public interface UserRepository extends JpaRepository<UserEntity, Integer> {

	/**
	 * Get an user with email.
	 *
	 * @param email an email.
	 * @return the found user.
	 */
	Optional<UserEntity> findByEmail(String email);

	/**
	 * Delete an user with email.
	 *
	 * @param email an email.
	 */
	void deleteByEmail(String email);

	/**
	 * Get List of {@code UserEntity} with this role.
	 *
	 * @param role the role.
	 * @return the List of {@code UserEntity}
	 */
	List<UserEntity> findByRole(Role role);

}
