/**
 *
 */
package com.management.bakery.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.management.bakery.exception.UserNotFoundException;
import com.management.bakery.model.dto.UserDto;
import com.management.bakery.service.impl.UserServiceImpl;

/**
 * @author jessy.sitcharn
 *
 */
@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/users")
public class UserController {

	/** User Service */
	@Autowired
	private UserServiceImpl userService;

	@GetMapping
	List<UserDto> findall() {
		return this.userService.findAllUser();
	}

	@GetMapping(value = "/{email}")
	UserDto findByEmail(@PathVariable String email) throws UserNotFoundException {
		return this.userService.getUser(email);
	}

	@DeleteMapping(value = "/{email}")
	void deleteByEmail(@PathVariable String email) throws UserNotFoundException {
		this.userService.deleteUser(email);
	}

	@PutMapping(value = "/{email}")
	UserDto update(@PathVariable String email, @RequestBody UserDto userDto) throws UserNotFoundException {
		return this.userService.updateUser(email, userDto);
	}
}
