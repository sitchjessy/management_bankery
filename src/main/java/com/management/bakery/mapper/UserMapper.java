/**
 *
 */
package com.management.bakery.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import com.management.bakery.model.dto.UserDto;
import com.management.bakery.model.entities.UserEntity;

/**
 * Interface managing mapping between {@code UserDto} and {@code UserEntity}.
 * 
 * @author jessy.sitcharn
 */
@Mapper
public interface UserMapper {

	UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

	/**
	 * Map a {@code UserEntity} to a {@code UserDto}.
	 *
	 * @param userEntity the {@code UserEntity} to map.
	 * @return the {@code UserDto};
	 */
	UserDto toUserDto(UserEntity userEntity);

	/**
	 * Map a {@code UserDto} to a {@code UserEntity}.
	 *
	 * @param userDto the {@code UserDto} to map.
	 * @return the {@code UserEntity}.
	 */
	UserEntity toUserEntity(UserDto userDto);

	/**
	 * Map a {@code List}<{@code UserEntity}> to a {@code List}<{@code UserDto}>.
	 *
	 * @param entities a {@code List}<{@code UserEntity}> to map.
	 * @return {@code List}<{@code UserDto}>.
	 */
	List<UserDto> toListEntity(List<UserEntity> entities);

	/**
	 * Update a {@code UserEntity}.
	 *
	 * @param userDto    the updated data.
	 * @param userEntity the Bean to update.
	 */
	void updateEntityFromDto(UserDto userDto, @MappingTarget UserEntity userEntity);

}
