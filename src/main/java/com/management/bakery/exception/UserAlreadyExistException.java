/**
 *
 */
package com.management.bakery.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author jessy.sitcharn
 *
 */
@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class UserAlreadyExistException extends Exception {

	/** Serializing parameter. */
	private static final long serialVersionUID = 1L;

	public UserAlreadyExistException(String message) {
		super(message);
	}

}
