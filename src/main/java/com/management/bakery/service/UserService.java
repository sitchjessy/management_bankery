/**
 *
 */
package com.management.bakery.service;

import java.util.List;

import com.management.bakery.exception.UserAlreadyExistException;
import com.management.bakery.exception.UserNotFoundException;
import com.management.bakery.model.dto.UserDto;

/**
 * @author jessy.sitcharn
 *
 */
public interface UserService {

	/**
	 * Get all User
	 *
	 * @return a List of {@code UserEntity}.
	 */
	List<UserDto> findAllUser();

	/**
	 * Save a user.
	 *
	 * @param userDto the {@link UserDto} to save.
	 * @return the saved UserDto.
	 */
	UserDto save(UserDto userDto) throws UserAlreadyExistException;

	/**
	 * Get a {@code UserDto}.
	 *
	 * @param email the email of the user to found.
	 * @return the found {@code UserDto}.
	 * @throws UserNotFoundException if we can't found a user with this email.
	 */
	UserDto getUser(String email) throws UserNotFoundException;

	/**
	 * Delete a user.
	 *
	 * @param email email of the user to delete.
	 * @throws UserNotFoundException if we can't found a user with this email.
	 */
	void deleteUser(String email) throws UserNotFoundException;

	/**
	 * Update a user.
	 *
	 * @param email   the email to found the user to update.
	 * @param userDto the new data.
	 * @return the updated {@code UserDto}.
	 * @throws UserNotFoundException if we can't found a user with this email.
	 */
	UserDto updateUser(String email, UserDto userDto) throws UserNotFoundException;
}
