/**
 *
 */
package com.management.bakery.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.management.bakery.exception.UserAlreadyExistException;
import com.management.bakery.exception.UserNotFoundException;
import com.management.bakery.mapper.UserMapper;
import com.management.bakery.model.dto.UserDto;
import com.management.bakery.model.entities.UserEntity;
import com.management.bakery.repositories.UserRepository;
import com.management.bakery.service.UserService;

/**
 * @author jessy.sitcharn
 *
 */
@Transactional
@Service("UserService")
public class UserServiceImpl implements UserService {

	/** DAO User Interface */
	@Autowired
	private UserRepository userRepository;

	@Autowired
	private PasswordEncoder bcryptEncoder;

	@Override
	public List<UserDto> findAllUser() {

		List<UserEntity> entities = this.userRepository.findAll();
		return UserMapper.INSTANCE.toListEntity(entities);
	}

	@Override
	public UserDto save(UserDto userDto) throws UserAlreadyExistException {

		if (!this.userRepository.findByEmail(userDto.getEmail()).isPresent()) {
			UserEntity userEntity = UserMapper.INSTANCE.toUserEntity(userDto);
			userEntity.setPassword(this.bcryptEncoder.encode(userDto.getPassword()));
			this.userRepository.save(userEntity);
			return UserMapper.INSTANCE.toUserDto(userEntity);
		} else {
			throw new UserAlreadyExistException("User with email " + userDto.getEmail() + " already exists.");
		}
	}

	@Override
	public UserDto getUser(String email) throws UserNotFoundException {

		UserEntity userEntity = this.userRepository.findByEmail(email)
				.orElseThrow(() -> new UserNotFoundException("Can't found user with email : " + email));

		return UserMapper.INSTANCE.toUserDto(userEntity);
	}

	@Override
	public void deleteUser(String email) throws UserNotFoundException {

		UserEntity userEntity = this.userRepository.findByEmail(email)
				.orElseThrow(() -> new UserNotFoundException("Can't found user with email : " + email));
		this.userRepository.delete(userEntity);
	}

	@Override
	public UserDto updateUser(String email, UserDto userDto) throws UserNotFoundException {
		UserEntity userEntity = this.userRepository.findByEmail(email)
				.orElseThrow(() -> new UserNotFoundException("Can't found user with email : " + email));

		UserMapper.INSTANCE.updateEntityFromDto(userDto, userEntity);
		this.userRepository.save(userEntity);
		return userDto;
	}

}
