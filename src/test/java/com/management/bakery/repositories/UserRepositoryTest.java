///**
// *
// */
//package com.management.bakery.repositories;
//
//import org.junit.jupiter.api.Assertions;
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
//import org.springframework.test.context.jdbc.Sql;
//
//import com.management.bakery.model.entities.Role;
//import com.management.bakery.model.entities.UserEntity;
//
///**
// * Testing {@link UserRepository}.
// *
// * @author jessy.sitcharn
// *
// */
//@DataJpaTest
//@Sql("dataUserRepoTest.sql")
//public class UserRepositoryTest {
//
//	@Autowired
//	private UserRepository userRepository;
//
//	@Test
//	void injectedComponentsAreNotNull() {
//		Assertions.assertNotNull(this.userRepository);
//	}
//
//	/**
//	 * Testing for {@link UserRepository#findByEmail(String)}.
//	 */
//	@Test
//	void testFindByEmail() {
//		UserEntity userEntity = this.userRepository.findByEmail("rob@test.fr").get();
//		Assertions.assertNotNull(userEntity);
//	}
//
//	/**
//	 * Testing for {@link UserRepository#deleteByEmail(String)}.
//	 */
//	@Test
//	void testDeleteByEmail() {
//		this.userRepository.deleteByEmail("rob@test.fr");
//		UserEntity userEntity = this.userRepository.findByEmail("rob@test.fr").get();
//		Assertions.assertNull(userEntity);
//	}
//
//	/**
//	 * Testing for {@link UserRepository#findByRole(String)}.
//	 */
//	@Test
//	void testFindByRole() {
//		Assertions.assertEquals(this.userRepository.findByRole(Role.GERANT).size(), 1);
//	}
//
//}
