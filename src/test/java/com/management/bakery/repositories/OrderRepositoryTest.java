///**
// *
// */
//package com.management.bakery.repositories;
//
//import java.text.ParseException;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//
//import org.junit.jupiter.api.Assertions;
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
//import org.springframework.test.context.jdbc.Sql;
//
///**/9*
// * Testing {@link OrderRepository}.
// *
// * @author jessy.sitcharn
// *
// */
//@DataJpaTest
//@Sql("dataOrderRepoTest.sql")
//public class OrderRepositoryTest {
//
//	@Autowired
//	private OrderRepository orderRepository;
//
//	@Test
//	void injectedComponentsAreNotNull() {
//		Assertions.assertNotNull(this.orderRepository);
//	}
//
//	/**
//	 * Testing for
//	 * {@link OrderRepository#findAllByDateBetween(Date, Date)} (java.util.Date, java.util.Date)}.
//	 *
//	 * We check the size of the list. We can test each elements if we ant be more
//	 * specific for this tests.
//	 *
//	 * @throws ParseException {@link ParseException}
//	 */
//	@Test
//	void testFindAllByDateBetween() throws ParseException {
//
//		Assertions.assertEquals(2,
//				this.orderRepository.findAllByDateBetween(new SimpleDateFormat("yyyy-MM-dd").parse("2014-01-01"),
//						new SimpleDateFormat("yyyy-MM-dd").parse("2014-01-20")).size());
//
//	}
//
//	/**
//	 * Testing for {@link OrderRepository#findByDate(java.util.Date)}.
//	 *
//	 * @throws ParseException
//	 */
//	@Test
//	void testFindByDate() throws ParseException {
//		Assertions.assertEquals(1,
//				this.orderRepository.findByDate(new SimpleDateFormat("yyyy-MM-dd").parse("2014-10-15")).size());
//	}
//
//}
