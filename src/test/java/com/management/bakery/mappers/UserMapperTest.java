package com.management.bakery.mappers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import com.management.bakery.mapper.UserMapper;
import com.management.bakery.model.dto.UserDto;
import com.management.bakery.model.entities.Role;
import com.management.bakery.model.entities.UserEntity;

/**
 * @author jessy.sitcharn
 *
 */
public class UserMapperTest {

	/**
	 * Testing {@link UserMapper#userEntityToUserDtor(UserEntity)}.
	 *
	 * @throws ParseException
	 */
	@Test
	public void mapUserEntityToUserDto() throws ParseException {

		UserEntity userEntity = new UserEntity(1, "Jessy", "Sitcharn", "jessy@test.fr", "4 rue saint jean 57000 Metz",
				"0665001454", new SimpleDateFormat("yyyy-MM-dd").parse("2018-01-15"), "jessyPassword", Role.BOULANGER);

		UserDto userDto = UserMapper.INSTANCE.toUserDto(userEntity);

		Assertions.assertNotNull(userDto);
		Assertions.assertEquals(userDto.getFirstName(), userEntity.getFirstName());
		Assertions.assertEquals(userDto.getLastName(), userEntity.getLastName());
		Assertions.assertEquals(userDto.getAddress(), userEntity.getAddress());
		Assertions.assertEquals(userDto.getPhoneNumber(), userEntity.getPhoneNumber());
		Assertions.assertEquals(userDto.getPassword(), userEntity.getPassword());
		Assertions.assertEquals(userDto.getStartDate(), userEntity.getStartDate());
		System.out.println(userDto.toString());
	}

	/**
	 * Testing {@link UserMapper#toUserEntity(UserDto)}.
	 *
	 * @throws ParseException
	 */
	@Test
	public void mapUserDtoToUserEntity() throws ParseException {

		UserDto userDto = new UserDto("Jessy", "Sitcharn", "jessy@test.fr", "4 rue saint jean 57000 Metz", "0665001454",
				new SimpleDateFormat("yyyy-MM-dd").parse("2018-01-15"), "jessyPassword");

		UserEntity userEntity = UserMapper.INSTANCE.toUserEntity(userDto);

		Assertions.assertNotNull(userEntity);
		Assertions.assertEquals(userEntity.getFirstName(), userDto.getFirstName());
		Assertions.assertEquals(userEntity.getLastName(), userDto.getLastName());
		Assertions.assertEquals(userEntity.getAddress(), userDto.getAddress());
		Assertions.assertEquals(userEntity.getPhoneNumber(), userDto.getPhoneNumber());
		Assertions.assertEquals(userEntity.getPassword(), userDto.getPassword());
		Assertions.assertEquals(userEntity.getStartDate(), userDto.getStartDate());
		System.out.println(userEntity.toString());
	}

	@Test
	public void maplistEntityToListDto() throws ParseException {

		UserEntity userEntity = new UserEntity(1, "Jessy", "Sitcharn", "jessy@test.fr", "4 rue saint jean 57000 Metz",
				"0665001454", new SimpleDateFormat("yyyy-MM-dd").parse("2018-01-15"), "jessyPassword", Role.BOULANGER),
				userEntity2 = new UserEntity(2, "James", "Sitcharn", "james@test.fr", "Maisoncelle", "0606060606",
						new SimpleDateFormat("yyyy-MM-dd").parse("2018-01-15"), "jamesPass", Role.EMPLOYE);

		List<UserEntity> entities = Arrays.asList(userEntity, userEntity2);

		List<UserDto> dtos = UserMapper.INSTANCE.toListEntity(entities);

		Assertions.assertNotNull(dtos);
		Assertions.assertEquals(entities.size(), dtos.size());
		for (UserDto userDto : dtos) {
			System.out.println(userDto.toString());
		}
		for (UserEntity userEntity3 : entities) {
			System.out.println(userEntity3.toString());
		}

	}

	/**
	 * Testing {@link UserMapper#updateEntityFromDto(UserDto, UserEntity)}.
	 *
	 * @throws ParseException
	 */
	@Test
	public void updateEntityFromDto() throws ParseException {

		UserEntity userEntity = new UserEntity(1, "Jessy", "Sitcharn", "jessy@test.fr", "4 rue saint jean 57000 Metz",
				"0665001454", new SimpleDateFormat("yyyy-MM-dd").parse("2018-01-15"), "jessyPassword", Role.BOULANGER);
		UserDto userDto = new UserDto("JessyJean", "Sitc", "jessyjean@test.fr", "Metz", "0590915988",
				new SimpleDateFormat("yyyy-MM-dd").parse("2018-01-15"), "jessyJeanPassword");

		UserMapper.INSTANCE.updateEntityFromDto(userDto, userEntity);

		Assertions.assertNotNull(userEntity);
		Assertions.assertEquals(userEntity.getFirstName(), userDto.getFirstName());
		Assertions.assertEquals(userEntity.getLastName(), userDto.getLastName());
		Assertions.assertEquals(userEntity.getAddress(), userDto.getAddress());
		Assertions.assertEquals(userEntity.getPhoneNumber(), userDto.getPhoneNumber());
		Assertions.assertEquals(userEntity.getPassword(), userDto.getPassword());
		Assertions.assertEquals(userEntity.getStartDate(), userDto.getStartDate());
		System.out.println(userEntity.toString());

	}
}
